function createCard(name, description, pictureUrl, startDate, startMonth, startYear, endDate, endMonth, endYear, location) {
    return `
      <div class=" shadow card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
              <small class="text-body-secondary">${startMonth}/${startDate}/${startYear} - ${endMonth}/${endDate}/${endYear}</small>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
          console.log('Unable to fetch this url...')
      } else {
        const data = await response.json();

        let counter = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts);
            const ends = new Date(details.conference.ends);
            const startDate = starts.getDate();
            const startMonth = starts.getMonth();
            const startYear = starts.getFullYear();
            const endDate = ends.getDate();
            const endMonth = ends.getMonth();
            const endYear = ends.getFullYear();
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, startMonth, startYear, endDate, endMonth, endYear, location);
            const column = document.querySelectorAll('.col');
            const col = column[counter];
            col.innerHTML += html;
            counter++;
            if (counter === 3) {
              counter = 0;
            }
          }
        }

      }
    } catch (e) {
        console.error(e);
      
    }

  });
